package com.example.playlist_exoplayer

import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {
    private lateinit var mediaItem: MediaItem
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val simpleExoPlayer = SimpleExoPlayer.Builder(applicationContext).build()
        exoPlayerView.player = simpleExoPlayer

        val path = "${Environment.getExternalStorageDirectory()}/Download"
        val listFile = File(path).listFiles()?.toList()
        val listSong: MutableList<MediaItem> = mutableListOf()

        if (listFile != null) {
            for (item in listFile.indices) {
                mediaItem = MediaItem.fromUri("${listFile[item]}")
                listSong.add(mediaItem)
            }
        }
        simpleExoPlayer.addMediaItems(listSong.toList())
        simpleExoPlayer.prepare()
        simpleExoPlayer.play()
    }

}