package com.example.testdaggerhilt

import android.util.Log
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import javax.inject.Inject

val TAG = "TestDaggerHilt"


interface Interceptor {
    fun log(message: String)
}

class CallInterceptorImpl @Inject constructor() : Interceptor {

    override fun log(message: String) {
        Log.d(TAG, "this is a call interceptor : $message")
    }
}

class ResponseInterceptorImpl @Inject constructor() : Interceptor {
    override fun log(message: String) {
        Log.d(TAG, "this is a response interceptor : $message")
    }
}























