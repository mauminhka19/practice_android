package com.example.testdaggerhilt.network

interface NetworkAdapter {
    fun log(message: String)
}