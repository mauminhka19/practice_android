package com.example.testdaggerhilt

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.testdaggerhilt.hilt.CallInterceptor
import com.example.testdaggerhilt.hilt.ResponseInterceptor
import com.example.testdaggerhilt.network.NetworkService
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @CallInterceptor
    @Inject
    lateinit var callNetworkService: NetworkService

    @ResponseInterceptor
    @Inject
    lateinit var responseNetworkService: NetworkService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        callNetworkService.performNetworkCall()
        responseNetworkService.performNetworkCall()
    }

}