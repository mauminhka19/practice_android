package com.example.demo

import retrofit2.Call
import retrofit2.http.GET

interface APIInterface {

    //https://jsonplaceholder.typicode.com/posts
    @GET("posts")
    fun getData() : Call<List<MyDataItem>>
}