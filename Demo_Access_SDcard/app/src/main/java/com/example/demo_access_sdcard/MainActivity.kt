package com.example.demo_access_sdcard

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import java.io.FileOutputStream
import java.util.*

class MainActivity : AppCompatActivity() {​​​​​​​​
    private val REQUEST_PERMISSION_CODE = 1
    private val REQUEST_SELECT_IMAGE_CODE = 2

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!checkingPermissionIsEnabledOrNot()) {
            requestMultiplePermission()
        }​​​​​​​​
        btn_save.setOnClickListener() {
            val bitmapDrawable: BitmapDrawable = img_View.drawable as BitmapDrawable
            val bitmap: Bitmap = bitmapDrawable.bitmap
            saveImageToGallery(bitmap)
        }​​​​​​​​
        btn_choose.setOnClickListener() {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, REQUEST_SELECT_IMAGE_CODE)
        }​​​​​​​​
    }

    private fun saveImageToGallery(bitmap: Bitmap) {
        val fos: FileOutputStream
        try {
            ​​​​​​​​
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                ​​​​​​​​
                val resolver: ContentResolver = contentResolver
                val contentValues = ContentValues().apply {
                    put(MediaStore.MediaColumns.DISPLAY_NAME, "Image_" + ".jpg")
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                    put(
                        MediaStore.MediaColumns.RELATIVE_PATH,
                        Environment.DIRECTORY_PICTURES
                    )
                }
                val imageUri: Uri? =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                fos = Objects.requireNonNull(imageUri)
                    ?.let { resolver.openOutputStream(it) } as FileOutputStream
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                Objects.requireNonNull(fos)
                Toast.makeText(this, "Image saved", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            Log.d("MainActivity", "Image not save")
        }
    }
    ​​​​​​​​
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SELECT_IMAGE_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                val selectedImageUri: Uri? = data.data
                if (selectedImageUri != null) {
                    ​​​​​​​​
                    val inputSteam = contentResolver.openInputStream(selectedImageUri)
                    val bitmap = BitmapFactory.decodeStream(inputSteam)
                    img_photo.setImageBitmap(bitmap)
                    Toast.makeText(
                        this,
                        "Loaded: " + getPathFromUri(selectedImageUri),
                        Toast.LENGTH_LONG
                    ).show()
                    Log.d("MainActivity123", getPathFromUri(selectedImageUri))
                }​​​​​​​​
            }​​​​​​​​
        }​​​​​​​​
    }​​​​​​​​
    @SuppressLint("Recycle")
    private fun getPathFromUri(contentUri: Uri): String {
        val filePath: String
        val cursor: Cursor? = contentResolver.query(contentUri, null, null, null, null)
        if (cursor != null) {
            filePath = contentUri.path!!
        }​​​​​​​​
    }​​​​​​​​

    private fun requestMultiplePermission() {
        ActivityCompat.requestPermissions(
            this@MainActivity, arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
            ), REQUEST_PERMISSION_CODE
        )
    }

    private fun checkingPermissionIsEnabledOrNot(): Boolean {
        val firstPermissionResult = ContextCompat.checkSelfPermission(
            applicationContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return firstPermissionResult == PackageManager.PERMISSION_GRANTED
    }
}​​​​​​​​
