package com.example.mvvmarchitecture.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class Country(

    @SerializedName("name")
    val countryName: String?,
    @SerializedName("capital")
    val capital: String?,
    @SerializedName("flagPNG")
    val flag: String?
)