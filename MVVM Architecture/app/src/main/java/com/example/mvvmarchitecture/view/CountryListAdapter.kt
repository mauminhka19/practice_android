package com.example.mvvmarchitecture.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmarchitecture.R
import com.example.mvvmarchitecture.model.Country
import com.example.mvvmarchitecture.util.getProgressDrawable
import com.example.mvvmarchitecture.util.loadImage
import kotlinx.android.synthetic.main.item_country.view.*

class CountryListAdapter(var country: ArrayList<Country>) :
    RecyclerView.Adapter<CountryListAdapter.CountryViewHolder>() {

    fun updateCountries(newCountry: List<Country>) {
        country.clear()
        country.addAll(newCountry)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        return CountryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_country, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(country[position])
    }

    override fun getItemCount(): Int {
        return country.size
    }

    class CountryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val countryName = view.name
        private val countryCapital = view.capital
        private val imageView = view.imageView
        private val progressDrawable = getProgressDrawable(view.context)
        fun bind(country: Country) {
            countryName.text = country.countryName
            countryCapital.text = country.countryName
            imageView.loadImage(country.flag, progressDrawable)
        }
    }

}