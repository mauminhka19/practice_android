package com.example.mvvmarchitecture.di

import com.example.mvvmarchitecture.model.CountriesService
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {
    fun inject(service: CountriesService)
}