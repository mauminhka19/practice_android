package com.example.play_video


import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset


class MainActivity : AppCompatActivity() {
    private val REQUEST_PERMISSION_CODE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (checkExternalMemoryAvailable(this)) {
            if (!checkingPermissionIsEnabledOrNot()) {
                requestMultiplePermission()
            } else {
               readJson()
            }
        } else Toast.makeText(this, "the device doesn't have a SD card", Toast.LENGTH_LONG).show()
    }

    override fun onResume() {
        super.onResume()
        readJson()
    }

    private fun readJson() {
        val storage = ContextCompat.getExternalFilesDirs(this, null)
        val sdName = storage[1].toString().split("/")[2]
        var json: String
        val charset: Charset = Charsets.UTF_8
        try {
            val myFile = File("/storage/$sdName/Property/iot_message_properties.json")
            val inputStream: InputStream = FileInputStream(myFile)
            val size: Int = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer, charset)
            val jsonObject = JSONObject(json)
            txt_show.text = jsonObject.getString("IotCore_Endpoint")
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    private fun checkExternalMemoryAvailable(context: Activity?): Boolean {
        val storage = ContextCompat.getExternalFilesDirs(context!!, null)
        return storage.size > 1 && storage[0] != null && storage[1] != null
    }
    private fun requestMultiplePermission() {
        ActivityCompat.requestPermissions(
            this@MainActivity, arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), REQUEST_PERMISSION_CODE
        )
    }
    private fun checkingPermissionIsEnabledOrNot(): Boolean {
        val firstPermissionResult = ContextCompat.checkSelfPermission(
            applicationContext,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        return firstPermissionResult == PackageManager.PERMISSION_GRANTED
    }

}